assert False, "start of file"
# START
prefix = 'BBB>'
# END
# START
print prefix+"""all
    these
    lines
    will
    be"""
# END
assert False, prefix+"between sections, midfile"
# START
print prefix+"section 2"
print
# END

# MOVE TO START OF FILE
assert False, prefix +"failed to detect "
# END OF FILE
