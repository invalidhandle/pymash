Use this to "mash" parts of separate files (specifically from python scripts) togother into one file.
Helps development process on 3rd party application that require a single-file plugin.