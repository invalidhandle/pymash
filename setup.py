from distutils.core import setup
setup(
  name = 'pymash',
  packages = ['pymash'], # this must be the same as the name above
  version = '0.4',
  description = 'Module for mashing together python scripts into one single file',
  author = 'invhndl',
  author_email = 'invalidhandle@outlook.com',
  url = 'https://bitbucket.org/invalidhandle/pymash',
  download_url = 'https://bitbucket.org/invalidhandle/pymash/get/0.4.tar.gz',
  keywords = ['merging', 'combining', 'mash'],
  classifiers = ['Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 3'],
  license='MIT'
)
